/*
 * stopswitch.h
 *
 *  Created on: 2019/09/13
 *      Author: tomok
 */

#ifndef STOPSWITCH_H_
#define STOPSWITCH_H_

//停止スイッチ
#define STOP_SW_PORT GPIOA
#define STOP_SW_PIN GPIO_PIN_12

//スタートスイッチ
#define START_SW_PORT GPIOA
#define START_SW_PIN GPIO_PIN_10

#endif /* STOPSWITCH_H_ */
