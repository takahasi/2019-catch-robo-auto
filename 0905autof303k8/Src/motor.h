/*
 * motor.h
 *
 *  Created on: 2019/09/05
 *      Author: tomok
 */

#ifndef MOTOR_H_
#define MOTOR_H_

//#include "math.h"
#include "stm32f3xx_hal.h"
//#include "struct.h"



/*-----------------------------------------------
 * 座標軸DCモータ(r, theta, z)
-----------------------------------------------*/
void CoordinateSetPosXYZ(void);
void CoordinateSetPosRTZ(void);
void CoordinateSetDuty(float duty);
void CoordinateEnable(void);
void CoordinateDisable(void);
void CoordiateReset(void);

//座標軸DCモータタイマハンドラ
#define COORDINATE_TIM_HANDLER		htim3

//PWMのARR値（MAXの値）
#define PWM_ARR 			10000.0f
//モータPWMチャンネル
#define PWM_TIM_CH				TIM_CHANNEL_4


//DCモータ回転方向GPIO
#define MOTOR_R_1_PORT				GPIOB
#define MOTOR_R_1_PIN				GPIO_PIN_1
#define MOTOR_R_2_PORT				GPIOA			//GPIOB
#define MOTOR_R_2_PIN				GPIO_PIN_11		//GPIO_PIN_6


//モータブレーキ
#define MOTOR_BRAKE					0.0f


#endif /* MOTOR_H_ */
