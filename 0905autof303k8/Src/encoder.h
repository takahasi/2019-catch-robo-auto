/*
 * encoder.h
 *
 *  Created on: 2019/09/05
 *      Author: tomok
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include "stm32f3xx_hal.h"

#define ENC_1_TIM_HANDLER	htim1
#define ENC_1_TIM	TIM1

extern TIM_HandleTypeDef htim1;

//右円筒のR軸の現在地を得るためのパラメータ
#define ENC_R_RESOLUTION		56
#define MOTOR_R_GEARHEAD		19
#define	R_PINION_DIAMETER		0.028f
#define M_PI 3.141592

//右円筒のθ軸の現在地を得るためのパラメータ
#define THITA_DEGREE			68		//エンコーダが一周するときにθ軸が回る角度
#define ENC_THITA_RESOLUTION	2400	//エンコーダの分解能600×4

//自動のPID制御のパラメータ
#define PID_P	1.5f
#define MD_DELTA_T 0.02f

//プロトタイプ宣言
void EncGetData(int16_t *);
void EncEnable(void);
void EncDisable(void);
float GetPos(void);
void PIDOut(float current_pos,float ref_pos);


#endif /* ENCODER_H_ */
