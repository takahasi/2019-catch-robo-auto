/*
 * servomotor.h
 *
 *  Created on: 2019/09/05
 *      Author: tomok
 */

#ifndef SERVOMOTOR_H_
#define SERVOMOTOR_H_

#include "stm32f3xx_hal.h"
#include "math.h"

long map(long x,long in_min,long in_max,long out_min,long out_max);
void ServoSetAngle(int angle,int servo_id);
void ServoDisable(void);
void ServoEnable(void);
void ServoGradAngle(int target_angle,int prev_angle,int speed,int delay,int servo_id);

//サーボの可動範囲(0.5ms ~ 2.0ms)

//自動の先のサーボ
#define AUTO_SERVO1_LOW		450
#define AUTO_SERVO1_HIGH		1250
#define AUTO_SERVO1_MAX_ANGLE	180
#define AUTO_SERVO1_MIN_ANGLE	-90

//自動の根本のサーボ
/*
#define AUTO_SERVO2_LOW		250
#define AUTO_SERVO2_HIGH		870
#define AUTO_SERVO2_MAX_ANGLE	180
#define AUTO_SERVO2_MIN_ANGLE	-90
*/
#define AUTO_SERVO2_LOW		250
#define AUTO_SERVO2_HIGH		870
#define AUTO_SERVO2_MAX_ANGLE	180
#define AUTO_SERVO2_MIN_ANGLE	-90


#define AUTO_SERVO1_ID 13
#define AUTO_SERVO2_ID 14




//サーボのタイマーハンドラ
#define AUTO_SERVO1_TIM_HANDLER	htim3
#define AUTO_SERVO2_TIM_HANDLER	htim3
//サーボのチャンネル
#define AUTO_SERVO1_TIM_CH	TIM_CHANNEL_1
#define AUTO_SERVO2_TIM_CH	TIM_CHANNEL_2


//サーボの初期位置
#define AUTO_SERVO1_ZERO_ANGLE 	170
#define AUTO_SERVO2_ZERO_ANGLE 	165

#endif /* SERVOMOTOR_H_ */
