/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "motor.h"
#include "encoder.h"
#include "servomotor.h"
#include "stopswitch.h"
#include "robotstate.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim6;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
static char s[255] = {'\0'};
int AUTO_SERVO1_angle = AUTO_SERVO1_ZERO_ANGLE;
int AUTO_SERVO2_angle = AUTO_SERVO2_ZERO_ANGLE;
int PREV_AUTO_SERVO1_angle = AUTO_SERVO1_ZERO_ANGLE;
int PREV_AUTO_SERVO2_angle = AUTO_SERVO2_ZERO_ANGLE;
int servo_flag = 0;
int prev_flag = 0;
float current_pos = 0.0f;
int count = 0;
int stop_flag = 0;
int start_flag = 0;
int switch_flag = 1;

//微調整用
int diff_2 = -15;
int diff_1 = 8;
int reset_diff = 7;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM6_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	//TIM6割り込み
	if (htim->Instance == TIM6) {
		current_pos = GetPos();
		//停止スイッチを押すと元の場所に戻る
		if(HAL_GPIO_ReadPin(STOP_SW_PORT, STOP_SW_PIN) == GPIO_PIN_RESET){
			stop_flag = 1;
		}
		//開始スイッチを押すとスタートする
		if(HAL_GPIO_ReadPin(START_SW_PORT, START_SW_PIN) == GPIO_PIN_RESET){
			start_flag = 1;
		}
		//マイクロスイッチが押されていると出力OFF
		if(HAL_GPIO_ReadPin(MICROSW_Y_MIN_PORT, MICROSW_Y_MIN_PIN) == GPIO_PIN_RESET){
			CoordinateSetDuty(0.0f);
		}

	}

}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_USART2_UART_Init();
  MX_TIM6_Init();
  /* USER CODE BEGIN 2 */
  //サーボ初期化
  ServoEnable();

  //モータ初期化
  CoordinateEnable();

  //マイクロスイッチで初期化
  ResetPos();

  //エンコーダー初期化
  EncEnable();

  HAL_Delay(300);

/*
  __HAL_TIM_SET_COMPARE(&COORDINATE_TIM_HANDLER, PWM_TIM_CH, 	5000);
  HAL_GPIO_WritePin(MOTOR_R_1_PORT, MOTOR_R_1_PIN, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(MOTOR_R_2_PORT, MOTOR_R_2_PIN, GPIO_PIN_SET);

  HAL_Delay(300);

  __HAL_TIM_SET_COMPARE(&COORDINATE_TIM_HANDLER, PWM_TIM_CH, 	0);
  HAL_GPIO_WritePin(MOTOR_R_1_PORT, MOTOR_R_1_PIN, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(MOTOR_R_2_PORT, MOTOR_R_2_PIN, GPIO_PIN_RESET);

*/

  /*
  AUTO_SERVO1_angle = 150;
  ServoSetAngle(AUTO_SERVO1_angle,AUTO_SERVO1_ID);
  HAL_Delay(1000);

  AUTO_SERVO2_angle = 150;
  ServoSetAngle(AUTO_SERVO2_angle,AUTO_SERVO2_ID);
  HAL_Delay(1000);
  */

  HAL_Delay(1000);

  //制御割り込み開始
  HAL_TIM_Base_Start_IT(&htim6);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  if(HAL_GPIO_ReadPin(MICROSW_Y_MIN_PORT, MICROSW_Y_MIN_PIN) == GPIO_PIN_RESET){
		  switch_flag = 0;
	  }else if(HAL_GPIO_ReadPin(MICROSW_Y_MIN_PORT, MICROSW_Y_MIN_PIN) == GPIO_PIN_SET){
		  switch_flag = 1;
	  }

	  float duty = 0.3f;
	  int servo_speed = 1;
	  int delay = 10;


	  //現在地を更新
	  current_pos = GetPos();

	  sprintf(s, "current_pos: %d servo_flag: %d prev_flag: %d servo1: %d servo2: %d PWM: %d SW: %d START_SW: %d STOP_SW: %d \r\n", (int)(current_pos*1000.0f),servo_flag,prev_flag,AUTO_SERVO1_angle,AUTO_SERVO2_angle,(int)(duty*PWM_ARR),switch_flag,start_flag,stop_flag);
	  HAL_UART_Transmit(&huart2,(uint8_t *)s, strlen(s), 100);

	  //スタートスイッチを押したら動作スタート
	  if(start_flag == 1){
		  //停止スイッチでフェイスを中断してリセットして止まる
		  if(stop_flag == 1){
			  StopPos(stop_flag);
			  servo_flag = 9;
			  prev_flag = 8;
			  CoordinateSetDuty(0.0f);
		  }


		  //phase1
		  if(servo_flag == 0 && prev_flag == 0){
				//1
			  servo_speed = 1;
			  delay = 5;
			  AUTO_SERVO2_angle = fminf(fmaxf(18 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO1_angle = fminf(fmaxf(30, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
			  //HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  servo_flag = 1;
			  prev_flag = 0;

		  }


		  //phase2
		  if(servo_flag == 1 && prev_flag == 0){
			  if((int)(current_pos*1000.0f) >  540 && (int)(current_pos*1000.0f) <  570){
				  CoordinateSetDuty(0.0f);
				  servo_flag = 2;
				  prev_flag = 1;
			  }else{
				  //CoordinateSetDuty(duty);
				  PIDOut(current_pos,0.543f);
				  servo_flag = 1;
				  prev_flag = 0;
			  }
		  }
		  sprintf(s, "current_pos: %d servo_flag: %d prev_flag: %d servo1: %d servo2: %d \r\n", (int)(current_pos*1000.0f),servo_flag,prev_flag,AUTO_SERVO1_angle,AUTO_SERVO2_angle);
		  HAL_UART_Transmit(&huart2,(uint8_t *)s, strlen(s), 100);

		  //phase3
		  if(servo_flag == 2 && prev_flag == 1){
			  if(current_pos >  0.54f && current_pos <  0.56f){
				//1
				  servo_speed = 1;
				  AUTO_SERVO2_angle = fminf(fmaxf(-6 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
				  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//				  HAL_Delay(500);

				  AUTO_SERVO1_angle = fminf(fmaxf(30 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
				  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//				  HAL_Delay(500);

				  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
				  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

				  servo_flag = 3;
				  prev_flag = 2;

			  }
		  }
		  sprintf(s, "current_pos: %d servo_flag: %d prev_flag: %d servo1: %d servo2: %d \r\n", (int)(current_pos*1000.0f),servo_flag,prev_flag,AUTO_SERVO1_angle,AUTO_SERVO2_angle);
		  HAL_UART_Transmit(&huart2,(uint8_t *)s, strlen(s), 100);

		  //phase4
		  if(servo_flag == 3 && prev_flag == 2){
			  if(current_pos >  0.560f && current_pos <  0.57f){
				  CoordinateSetDuty(0.0f);
				  servo_flag = 4;
				  prev_flag = 3;
			  }else{
				  duty = 0.2f;
				  CoordinateSetDuty(duty);
				  //PIDOut(current_pos,0.567f);
				  servo_flag = 3;
				  prev_flag = 2;
			  }
		  }
		  //phase5
		  if(servo_flag == 4 && prev_flag == 3){
			  //1
			  AUTO_SERVO1_angle = fminf(fmaxf(21 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-6 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //2
			  AUTO_SERVO1_angle = fminf(fmaxf(21 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-30 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //3
			  AUTO_SERVO1_angle = fminf(fmaxf(61 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-30 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //4
			  AUTO_SERVO1_angle = fminf(fmaxf(73 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-30 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //5
			  AUTO_SERVO1_angle = fminf(fmaxf(73 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-87 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //6
			  AUTO_SERVO1_angle = fminf(fmaxf(102 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-87 -diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //7
			  AUTO_SERVO1_angle = fminf(fmaxf(102 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-68 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //8
			  AUTO_SERVO1_angle = fminf(fmaxf(140 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(-68 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  servo_flag = 5;
			  prev_flag = 4;

		  }
		  //phase6
		  if(servo_flag == 5 && prev_flag == 4){
			  if(current_pos >  0.43f && current_pos <  0.46f){
				  CoordinateSetDuty(0.0f);
				  servo_flag = 6;
				  prev_flag = 5;
			  }else{
				  duty = 0.2f;
				  CoordinateSetDuty(-duty);
				  //PIDOut(current_pos,0.439f);
				  servo_flag = 5;
				  prev_flag = 4;
			  }
		  }

		  sprintf(s, "current_pos: %d servo_flag: %d prev_flag: %d servo1: %d servo2: %d \r\n", (int)(current_pos*1000.0f),servo_flag,prev_flag,AUTO_SERVO1_angle,AUTO_SERVO2_angle);
		  HAL_UART_Transmit(&huart2,(uint8_t *)s, strlen(s), 100);

		  //phase7
		  if(servo_flag == 6 && prev_flag == 5){
				//1
			  AUTO_SERVO2_angle = fminf(fmaxf(56 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO1_angle = fminf(fmaxf(140 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  //2
			  AUTO_SERVO1_angle = fminf(fmaxf(30 - diff_1, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);
//			  HAL_Delay(500);

			  AUTO_SERVO2_angle = fminf(fmaxf(18 - diff_2, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
//			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  if(count < 2){
				  //2へジャンプする
				  servo_flag = 1;
				  prev_flag = 0;
				  count++;
			  }else if(count == 2){
				  servo_flag = 7;
				  prev_flag = 6;
			  }
		  }



			//サーボリセット
		if(servo_flag == 7 && prev_flag == 6){
				//1


			  AUTO_SERVO1_angle = fminf(fmaxf(AUTO_SERVO1_ZERO_ANGLE, AUTO_SERVO1_MIN_ANGLE),AUTO_SERVO1_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO1_angle,PREV_AUTO_SERVO1_angle,servo_speed,delay,AUTO_SERVO1_ID);

			  AUTO_SERVO2_angle = fminf(fmaxf(AUTO_SERVO2_ZERO_ANGLE + reset_diff, AUTO_SERVO2_MIN_ANGLE),AUTO_SERVO2_MAX_ANGLE);
			  ServoGradAngle(AUTO_SERVO2_angle,PREV_AUTO_SERVO2_angle,servo_speed,delay,AUTO_SERVO2_ID);
			  //HAL_Delay(500);

			  HAL_Delay(500);

			  PREV_AUTO_SERVO1_angle = AUTO_SERVO1_angle;
			  PREV_AUTO_SERVO2_angle = AUTO_SERVO2_angle;

			  servo_flag = 8;
			  prev_flag = 7;
			}
		/*
		if(servo_flag == 8 && prev_flag == 7){
			if(current_pos >  0.434f && current_pos <  0.444f){
				CoordinateSetDuty(0.0f);
				servo_flag = 9;
				prev_flag = 8;
			}else{
				//CoordinateSetDuty(-duty);
				PIDOut(current_pos,0.01f);
				servo_flag = 8;
				prev_flag = 7;
			}
		}
		*/
		//全てのフェイズを過ぎると元の場所に戻って停止する
		if(servo_flag == 8 && prev_flag == 7){
			StopPos(1);
		}
	  }




    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_TIM1;
  PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLK_HCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 15;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 9999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 15;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 9999;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 15;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 9999;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, MD_INA_Pin|LD3_Pin|MD_INB_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);

  /*Configure GPIO pins : MicroSW_Pin START_SW_Pin STOP_SW_Pin */
  GPIO_InitStruct.Pin = MicroSW_Pin|START_SW_Pin|STOP_SW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : MD_INA_Pin LD3_Pin MD_INB_Pin */
  GPIO_InitStruct.Pin = MD_INA_Pin|LD3_Pin|MD_INB_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PA11 */
  GPIO_InitStruct.Pin = GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
