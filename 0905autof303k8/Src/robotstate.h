/*
 * robotstate.h
 *
 *  Created on: 2019/09/13
 *      Author: tomok
 */

#ifndef ROBOTSTATE_H_
#define ROBOTSTATE_H_

#define MICROSW_Y_MIN_PORT	GPIOA
#define MICROSW_Y_MIN_PIN	GPIO_PIN_7

void ResetPos(void);

#endif /* ROBOTSTATE_H_ */
